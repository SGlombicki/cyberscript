$path = "C:\CyberPatriot"
If(!(test-path -PathType container $path))
{
      New-Item -ItemType Directory -Path $path
}

if(-not(Test-Path -Path "C:\CyberPatriot\vmware.exe" -PathType Any))
{
    Invoke-WebRequest -Uri "https://download3.vmware.com/software/WKST-PLAYER-1624/VMware-player-full-16.2.4-20089737.exe" -OutFile "C:\CyberPatriot\vmware.exe"
    C:\CyberPatriot\vmware.exe /s /v/qn EULAS_AGREED=1
}

if(-not(Test-Path -Path "C:\CyberPatriot\cisco.exe" -PathType Any))
{
    Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/39426706/repository/files/CiscoPacketTracer_820_Windows_64bit.exe/?ref=main" -OutFile "C:\CyberPatriot\cisco.exe"
    C:\CyberPatriot\cisco.exe /VERYSILENT /NORESTART
}
